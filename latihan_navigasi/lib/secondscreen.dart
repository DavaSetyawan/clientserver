import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

class SecondScreen extends StatelessWidget {
 
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(children: [
          Text("Ini adalah Layar Kedua"),
          ElevatedButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text("Kembali"))
        ]),
      ),
    );
  }
}
