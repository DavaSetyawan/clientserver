import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:latihan_navigasi/firstscreen.dart';

class ThirdScreen extends StatelessWidget {
 
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(children: [
          Text("Ini adalah Layar Ketiga"),
          ElevatedButton(
              onPressed: () {
                Navigator.push(context,
                MaterialPageRoute(builder: (context)
                => FirstScreen()));
              },
              child: Text("Kembali"))
        ]),
      ),
    );
  }
}
